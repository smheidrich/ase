============================================================================
ASE Workshop: Software Development for Atomic Scale Modeling - Chalmers 2019
============================================================================

.. note::

    Up-to-date information is on `the workshop page <https://ase-workshop.materialsmodeling.org>`__.

The ASE package has seen continuous growth over several years and is now employed and supported by a very active community of users and developers.
This fall, we are therefore organizing a workshop that will be held at `Chalmers University of Technology <https://www.chalmers.se/en>`__ in `Gothenburg, Sweden <https://www.goteborg.com/en>`__ from **November 19-22, 2019**.
The objectives are to

* gather the community of *both* users and developers
* present new features
* showcase packages built on ASE
* discuss future developments

The workshop will feature

* presentations by developers
* contributed posters
* a hackathon

as well as customized tutorials that allow users to learn about more advanced applications.

The workshop is open to doctoral and Master students, post-doctoral as well as senior researchers who are interested in software for atomic scale simulation in general and the ASE package in particular.


Program
=======

See final program on `the workshop page <https://ase-workshop.materialsmodeling.org>`__.

Tentative schedule
------------------

.. role:: time
.. role:: free
.. role:: talk
.. role:: poster
.. role:: hack
.. role:: tuto
.. role:: social

.. raw:: html

 <script type="text/javascript">
   $('table.program').ready(function() {
     $('table.program .time').parent().parent().css('background-color', '#fefefe');
     $('table.program .free').html('');
     $('table.program .free').parent().parent().css('background-color', '#fefefe');
     $('table.program .talk').parent().parent().css('background-color', '#e41a1c80');
     $('table.program .poster').parent().parent().css('background-color', '#984ea380');
     $('table.program .hack').parent().parent().css('background-color', '#377eb880');
     $('table.program .tuto').parent().parent().css('background-color', '#ff7f0080');
     $('table.program .social').parent().parent().css('background-color', '#4daf4a80');
   });
 </script>

 <style type="text/css">
    table.program,
    table.program th,
    table.program td,
    table.program tr {
      border: 2px solid #e0e0e0;
      text-align: center;
    }
    table.program th {
      background-color: #b0b0b080;
    }
 </style>

.. table::
 :class: program

 +---------------+-----------------------+--------------------------+-------------------+-------------------+-------------------+-------------------+
 |               | Tuesday 19 Nov        | Wednesday 20 Nov         | Thursday 21 Nov                       | Friday 22 Nov                         |
 +===============+=======================+==========================+===================+===================+===================+===================+
 | :time:`9:00`  | :free:`-`             | :talk:`Invited talks`    | :hack:`Hackathon` | :tuto:`Tutorials` | :hack:`Hackathon` | :tuto:`Tutorials` |
 +---------------+                       +                          +                   +                   +                   +                   +
 | :time:`10:00` |                       |                          |                   |                   |                   |                   |
 +---------------+                       +                          +                   +                   +                   +                   +
 | :time:`11:00` |                       |                          |                   |                   |                   |                   |
 +---------------+-----------------------+--------------------------+-------------------+-------------------+-------------------+-------------------+
 | :time:`12:00` | :social:`Welcome`     | :social:`Lunch break`    | :social:`Lunch break`                 | :social:`Closing`                     |
 +---------------+-----------------------+--------------------------+-------------------+-------------------+-------------------+-------------------+
 | :time:`13:00` | :talk:`Invited talks` | :talk:`Invited talks`    | :hack:`Hackathon` | :tuto:`Tutorials` | :free:`-`                             |
 +---------------+                       +                          +                   +                   +                                       +
 | :time:`14:00` |                       |                          |                   |                   |                                       |
 +---------------+                       +                          +                   +                   +                                       +
 | :time:`15:00` |                       |                          |                   |                   |                                       |
 +---------------+                       +                          +                   +                   +                                       +
 | :time:`16:00` |                       |                          |                   |                   |                                       |
 +---------------+                       +                          +                   +                   +                                       +
 | :time:`17:00` |                       |                          |                   |                   |                                       |
 +---------------+-----------------------+--------------------------+-------------------+-------------------+                                       +
 | :time:`18:00` | :free:`-`             | :poster:`Poster session` | :free:`-`                             |                                       |
 +---------------+                       +                          +                                       +                                       +
 | :time:`19:00` |                       |                          |                                       |                                       |
 +---------------+                       +                          +                                       +                                       +
 | :time:`20:00` |                       |                          |                                       |                                       |
 +---------------+-----------------------+--------------------------+-------------------+-------------------+-------------------+-------------------+


Registration
============

Registration is closed.


Organizers
==========

* Paul Erhart, Chalmers University of Technology, Sweden
* Tuomas Rossi, Chalmers University of Technology, Sweden
* Ask Hjorth Larsen, Simune Atomistics S.L., Spain
* Jens Jørgen Mortensen, Technical University of Denmark, Denmark
* Kristian Sommer Thygesen, Technical University of Denmark, Denmark

For questions, please contact the organizers at ase-workshop@materialsmodeling.org.
